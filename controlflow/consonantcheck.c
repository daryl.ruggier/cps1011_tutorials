#include <stdio.h>
#include <string.h>

int isVowel(char str[]) {
    if ((int) str == 65 || (int) str == 69 || (int) str == 73 || (int)str == 79 || (int) str == 85 || (int) str == 97 || (int) str == 101 || (int) str == 105 || (int) str == 111 || (int) str == 117) {
        return 1;
    } //else return 0;
}

int isCapital(char str[]) {
    if ((int) str >= 65 && (int) str <= 90) {
        return 1;
    } else {
        return 0;
    }
}

int main (void) {
    char str[20];
    printf("Enter a string:\n");
    scanf("%s", str);

    for (int i = 0; str[i] != '\0'; i++) {
        if (!isCapital(str[i]) && !isVowel(str[i]) && (int) str[i] != 32) {
            str[i] = (int) str[i] - 32;
        }
    }
    printf("%s\n", str);

    return 0;

}
#include <stdio.h>
#define ARRAY_SIZE 20

float median(int n, int x[]) {
    int temp;
    int i, j;
    // the following two loops sort the array x in ascending order
    for (i = 0; i < n - 1; i++) {
        for (j = i + 1; j < n; j++) {
            if (x[j] < x[i]) {
                // swap elements
                temp = x[i];
                x[i] = x[j];
                x[j] = temp;
            }
        }
    }
    return 0;
}

int main (void) {
    int num[ARRAY_SIZE];
    int max = 0;
    int total = 0;
    setvbuf(stdout, NULL, _IONBF, 0);
    for (int i = 0; i < ARRAY_SIZE; i++) {
        do {
            printf("%d: Enter a number, the number inputted must be in the range of 1-100 and larger than the previous number inputted:\n", i+1);
            scanf("%d", &num[i]);
        } while (num[i] < 1 || num[i] > 100 || num[i] < max);
        if (num[i] > max) {
            max = num[i];
        }
        total+=num[i];
    }
    float med = median(ARRAY_SIZE, num);
    printf("Mean = %.2f\n", (double) total / ARRAY_SIZE);
    printf("Median = %.2f\n", med);
    return 0;
}


#include <math.h>
#include <stdio.h>

int riemannzetafunction (void) {
    float s;
    double total = 0;
    do {
        printf("Enter s: (only 1.5, 2, 3 and 4 allowed)\n");
        scanf("%f", &s);
    } while(s != 1.5 && s != 2 && s != 3 && s != 4);

    for (int i = 1; i <= 1000000; i++) {
        total+=pow(i, -s);
    }

    printf("zeta(%.1f) = %lf\n", s, total);



    return 0;
}
#include <Stdio.h> //for exit function
#include <stdlib.h>
#define PIN 1234
#define TRIES 3 //making use preprocessor macros
int pincheck (void) {
    int guess;
    for (int i = 1; i <= TRIES; i++) {
        printf("Guess the 4-digit pin: \n");
        scanf("%d", &guess);
        if (guess == PIN) {
            printf("Correct\n");
            exit(0); //program exits once the pin has been guessed
        } else {
            printf("Incorrect, you have %d more attempt(s):\n", TRIES - i);
        }
    }
    printf("You did not correctly guess the pin in %d tries. The correct pin was %d.\n", TRIES, PIN);
    return 0;
}
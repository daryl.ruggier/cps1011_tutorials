#include <stdio.h>
#include <string.h>
#define NUM_ITERATIONS 20

int main (void) {
    int num;
    int fact = 1;
    printf("Enter a number:\n");
    scanf("%d", &num);

    if (num >= 0) {
        for (int i = num; i >= 1; i--) {
        fact *= i;
        }
        printf("%d! = %d\n", num, fact);
    } else {
        printf("Number entered <= zero\n");
        main();
    }
    return 0;
}

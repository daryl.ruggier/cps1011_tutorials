#include <stdio.h>
int main (void) {
    int num;
    printf("Input a number:\n");
    scanf("%d", &num); //instead of limiting to only 1 through 10 in 10 rows, i set the program to output as many rows as the user would like

    for (int i = 1; i <= num; i++) { //multiplication table loop here, nested loop
        for (int j = 1; j <= num; j++) {
            printf("%d ", j * i);
        }
        printf("\n");
    }
    return 0;
}
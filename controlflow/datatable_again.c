#include <stdio.h>
#define ARRAY_SIZE 3
#define MAX_CHARS 10

int datatable_again (void) {
    setvbuf(stdout, NULL, _IONBF, 0);
    char name[ARRAY_SIZE][MAX_CHARS]; //2d arrays for strings
    char surname[ARRAY_SIZE][MAX_CHARS];
    int age[ARRAY_SIZE];
    float height[ARRAY_SIZE];

    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("Enter name for user %d:\n", i+1);
        scanf("%s", &name[i]);
        printf("Enter surname for user %d:\n", i+1);
        scanf("%s", &surname[i]);
        do {
            printf("Enter age for user %d:\n", i+1);
            scanf("%d", &age[i]);
        } while (age[i] <= 13 || age[i] >= 60); //sensible values
        do {
            printf("Enter height for user %d:\n", i+1);
            scanf("%f", &height[i]);
        } while (height[i] <= 1.5 || height[i] >= 2.1); //sensible values
    }
    setvbuf(stdout, NULL, _IONBF, 0);
    printf("+--------+---------+-----+--------+\n");
    printf("\n");
    printf("| Name | Surname | Age | Height |\n");
    printf("+--------+---------+-----+--------+\n");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("| %s | %s | %d | %.2f | \n", name[i], surname[i], age[i], height[i]);
    }
    printf("+--------+---------+-----+--------+\n");
    printf("Average age = %d\n", (age[0] + age[1] + age[2])/3);
    printf("Average height = %.2f\n", (height[0] + height[1] + height[2]) /3);

    return 0;
}
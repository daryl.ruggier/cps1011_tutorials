#include <stdio.h>
#define X 3
#define Y 5

int main (void) {
    double nums[X][Y];
    double avg[4]; //problem with doubles?
    int max = 0;
    for (int i = 0; i < X; i++) { //inputting the numbers
        printf("Enter a set of 5 doubles\n");
        scanf_s("%lf %lf %lf %lf %lf", &nums[i][0], &nums[i][1], &nums[i][2], &nums[i][3], &nums[i][4]);
        avg[i] = (nums[i][0] + nums[i][1] + nums[i][2] + nums[i][3] + nums[i][4]) / 5;

        for (int j = 0; j < X; j++) { //checking max num
            for (int k = 0; k < X; k++) {
                if (nums[j][k] >= max) {
                    max = (int) nums[j][k];
                }
            }
        }
    }
    avg[3] = (avg[0] + avg[1] + avg[2]) / 3;
    printf("Average of set 1: %.1lf \nAverage of set 2: %.1lf \nAverage of set 3: %.1lf \nAverage of all sets: %.1lf\n", avg[0], avg[1], avg[2], avg[3]);
    printf("Max = %d\n", max);
    return 0;
}
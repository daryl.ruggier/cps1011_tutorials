#include <stdio.h>
#pragma once

int copyArray(int arr[], size_t size) {
    int copy[100];
    printf("size of copy = %d\n", sizeof(copy));

    for (int i = 0; i < size; i++) {
        copy[i] = arr[i];
        printf("%d ", copy[i]);
    }

    return 0;
}
#include <stdio.h>

#define NUM_EMPLOYEES 3
#define MAX_NAME_CHARS 20
#define MAX_SURNAME_CHARS 20

struct employee {
    char name[MAX_NAME_CHARS];
    char surname[MAX_SURNAME_CHARS];
    int id;
    float salary;
};

typedef struct employee employee_t;
typedef employee_t *employee_ptr_t;

int main (void) {
    employee_t employees[NUM_EMPLOYEES];

    employee_t *surname_index[NUM_EMPLOYEES];
    employee_t *id_index[NUM_EMPLOYEES];

    for (int i = 0; i < NUM_EMPLOYEES; i++) {
        surname_index[i] = &employees[i];
        id_index[i] = &employees[i];
    }
}
#include <stdio.h>
#include <inttypes.h>

union ip {
    struct {
        char a, b, c, d;
    };
    //uint32_t val; not sure how to utilise
};

typedef union ip ipv4address;

int main(void) {
    setvbuf(stdout, NULL, _IONBF, 0);
    ipv4address ia;

    int a, b, c , d;

    printf("Enter I.P. address:\n");
    scanf("%d.%d.%d.%d", &a, &b, &c, &d);

    ia.a = (char)a;
    ia.b = (char)b;
    ia.c = (char)c;
    ia.d = (char)d;



    printf("%d.%d.%d.%d\n", ia.a, ia.b, ia.c, ia.d);
    printf("%02X%02X%02X%02X", ia.a, ia.b, ia.c, ia.d);

    return 0;
}
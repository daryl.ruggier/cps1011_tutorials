/* fgets1.c -- using fgets() and fputs() */
#include <stdio.h>

#define STLEN 14

char *sgets();
int main(void)
{
    char words[STLEN];

    puts("Enter a string, please.");
    sgets(words, STLEN, stdin);
    printf("Your string twice (puts(), then fputs()):\n");
    puts(words);
    fputs(words, stdout);
    puts("Enter another string, please.");
    sgets(words, STLEN, stdin);
    printf("Your string twice (puts(), then fputs()):\n");
    puts(words);
    fputs(words, stdout);
    puts("Done.");

    return 0;
}

char *sgets (char *str, int size) {
    char *ret;
    ret = fgets(str, size, stdin);

    if (!ret) {
        return ret;
    }

    if (str[sizeof(str) -1] == '\n') {
        str[sizeof(str) -1] = '\0';
    } else {
        while(getchar() != '\n');
    }
    return ret;

}
#include <stdio.h>

int main (int argc, char **argv) {
    if (argc != 3) { //check that only 3 command line arguments are inputted
        printf("Wrong usage\n");
        return 1;
    }

    int n1, n2;
    int ret1, ret2;

    ret1 = sscanf(argv[1], "%d", &n1); //check if it's the right type
    ret2 = sscanf(argv[2], "%d", &n2);

    if (ret1 != 1 || ret2 != 1) {
        printf("Wrong type");
        return 1;
    }

    printf("%d + %d = %d\n", n1, n2, n1+n2);


}
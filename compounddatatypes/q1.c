#include <stdio.h>
#include "q1.h"

int main(void) {
    int nums[5] = {2, 1, 22, 22, 22};
    size_t size = sizeof(nums) / 4;
    printf("size of nums = %d\n", size);
    copyArray(nums, size);

    return 0;
}

// copy of array of integers
// 1. create a dummy array of same size, make function var size array
// 2. just loop
// 3. test it in main i suppose


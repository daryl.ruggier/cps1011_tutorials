#include <stdio.h>
#include <string.h>
#define MAXLENGTH 20

int main (void) {
    char str[MAXLENGTH];
    char suffix[MAXLENGTH];

    printf("Enter a string\n");
    scanf("%s", str);

    printf("Enter the suffix\n");
    scanf("%s", suffix);

    strcat(str, suffix);

    printf("%s\n", str);

    return 0;
}

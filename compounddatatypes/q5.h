#pragma once
#include <stdio.h>

int compute(int x, int y, int z) {
    return (x + y + z);
}

int run(void) {
    //int arr[101][101][101];
    for (int x = 0; x < 101; x++) {
        for (int y = 0; y < 101; y++) {
            for (int z = 0; z < 101; z++) {
                //compute(x, y, z);
                //arr[x][y][z] = total;
                //arr[x][y][z] = compute(x, y, z);
                printf("arr[%d][%d][%d] = %d\n", x, y, z, compute(x, y, z));
            }
        }
    }
    return 0;
}
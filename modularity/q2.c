#include <stdio.h>
#include "q2.h"
int main (void) {
    int n;
    printf("Enter an integer to be converted to hex: \n");
    scanf("%d", &n);

    convertHex(n);
    printf("\nThe hex conversion of %d is %X\n", n, n);
    return 0;
}
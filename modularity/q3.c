#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main (void) {;
    setvbuf(stdout, NULL, _IONBF, 0);
    char input;
    int select, quantity, cont;
    float price[3], total = 0;
    _Bool selectCheck = 0;
    char order[10];

    price[0] = 1.50f;
    price[1] = 2;
    price[2] = 0.7f;

    do {
        printf("1. Add items to shopping cart\n");
        printf("2. Show current total\n");
        printf("3. Checkout\n");
        printf("4. Cancel session\n");
        printf("q. Quit\n");
        scanf("%c", &input);

        switch ((char) input) {
            case '1':
                selectCheck = 1;
                printf("1 - Cheese - $1.50\n");
                printf("2 - Bread  - $2.00\n");
                printf("3 - Milk   - $0.70\n");
                scanf("%d", &select);

                if (select == 1) {
                    strcpy(order, "Cheese");
                } else if (select == 2) {
                    strcpy(order, "Bread");
                } else if (select == 3) {
                    strcpy(order, "Milk");
                }

                printf("Enter quantity:\n");
                scanf("%d", &quantity);
                total = (quantity * price[select-1]); //+=

                printf("");
                break;
            case '2':
                if (!selectCheck) {
                    printf("You must first add items to the shopping cart!\n");
                    printf("\n");
                    main();
                }
                printf("Total = $%.2f\n", total);
                break;
            case '3':
                if (!selectCheck) {
                    printf("You must first add items to the shopping cart!\n");
                    main();
                }
                printf("Once you check out, you may not change your order. Do you want to continue? 'Y' or 'N'\n");
                scanf("%c", &cont);
                if (!strcmp(cont, "y") || !strcmp(cont, "yes") || !strcmp(cont, "Y") || !strcmp(cont, "Yes")) {
                    continue;
                } else {
                    printf("Products ordered:\n");
                    printf("%s\nQuantity: %d\nTotal: $%.2f\n", order, quantity, total);
                    printf("This session will be re-started.\n");
                    printf("\n");
                    main();
                }
                break;
            case '4':
                printf("Once you cancel the session, the previous session will be lost. Do you want to continue? Yes = 1, No = 0\n");
                scanf("%d", &cont);
                if (!cont) {
                    main();
                } else {
                    exit(0);
                }
                break;

            case 'q':
                printf("Terminating program\n");
                exit(0);
            default:
                printf("Invalid input\n");
                printf("\n");
                continue;
        }
    } while (input != 'q');

    return 0;
}
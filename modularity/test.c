#include <stdio.h>
#include <string.h>
#include <ctype.h>
void process_string(char s[], int *totalA, int *totalD, int *totalP, int *totalS) {
    int count;
    for (int i = 0; i < strlen(s); i++) {
        while (isalpha(s[i]) != 0) {
            count++;
            *totalA += 1;
     if (i == (strlen(s) - 1)) {
         printf("%da ", count);
         count = 0;
     } else if (isalpha(s[i + 1]) == 0) {
              printf("%da ", count);
     }
     count = 0;
     }
     break;
     }
 while (isdigit(s[i]) != 0) {
 count++;
 *totalD += 1;
 if (i == (strlen(s) - 1)) {
 printf("%dd ", count);
 count = 0;
 } else if (isdigit(s[i + 1]) == 0) {
 printf("%dd ", count);
 count = 0;
 }
 break;
 }
 while (ispunct(s[i]) != 0) {
 count++;
 *totalP += 1;
 if (i == (strlen(s) - 1)) {
 printf("%dp ", count);
 count = 0;
 } else if (ispunct(s[i + 1]) == 0) {
 printf("%dp ", count);
 count = 0;
 }
 break;
 }
 while (isspace(s[i]) != 0) {
 count++;
 *totalS += 1;
 if (i == (strlen(s) - 1)) {
 printf("%ds ", count);
 count = 0;
 } else if (isspace(s[i + 1]) == 0) {
 printf("%ds ", count);
 count = 0;
 }
 break;
 }
 }
 }
 int main() {
 setvbuf(stdout, NULL, _IONBF, 0);
 char str[100] = "";
 int totalA = 0;
 int totalD = 0;
 int totalP = 0;
 int totalS = 0;
 while (0 < 1) {
 printf("Enter String: ");
 fgets(str, 100, stdin);
 strtok(str, "\n");
 if (strcmp(str, "quit") == 0) {
 break;
 }else {
 process_string(str, &totalA, &totalD, &totalP, &totalS);
 printf("(Totals: %da %dd %dp %ds)\n\n", totalA, totalD, totalP, totalS);
 }
 }
 return 0;
 }
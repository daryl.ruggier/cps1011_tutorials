#include <stdio.h>
int datatable (void) {
    setvbuf(stdout, NULL, _IONBF, 0);
    char name[3][10];
    char surname[3][10];
    int age[3];
    float height[3];

    for (int i = 0; i < 3; i++) {
        printf("Enter name for user %d:\n", i+1);
        scanf("%s", &name[i]);
        printf("Enter surname for user %d:\n", i+1);
        scanf("%s", &surname[i]);
        printf("Enter age for user %d:\n", i+1);
        scanf("%d", &age[i]);
        printf("Enter height for user %d:\n", i+1);
        scanf("%f", &height[i]);

    }
    setvbuf(stdout, NULL, _IONBF, 0);
    printf("+--------+---------+-----+--------+\n");
    printf("\n");
    printf("| Name | Surname | Age | Height |\n");
    printf("+--------+---------+-----+--------+\n");
    for (int i = 0; i < 3; i++) {
        printf("| %s | %s | %d | %.2f | \n", name[i], surname[i], age[i], height[i]);
    }
    printf("+--------+---------+-----+--------+\n");
    printf("Average age = %d\n", (age[0] + age[1] + age[2])/3);
    printf("Average height = %.2f\n", (height[0] + height[1] + height[2]) /3);

    return 0;
}
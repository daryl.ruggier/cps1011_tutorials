#include <stdio.h>
#include <string.h>

int palindromer ċ1(void) {
    setvbuf(stdout, NULL, _IONBF, 0);

    char str[40];
    printf("Enter a string: ");
    scanf("%s", str);

    printf("%s", str);
    printf("%s\n", strrev(str)); //Doesn't allow for the program to output str and strrev in a single line?
    return 0;
}
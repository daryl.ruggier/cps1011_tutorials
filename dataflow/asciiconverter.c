#include <stdio.h>

    int asciiconverter (void) {
    int input = -1; //"garbage values from stack"
    printf("Enter a number\n");
    scanf("%d", &input);

    printf("The ascii equivalent of %d is %c\n", input, input);
    printf("The decimal equivalent of %d is %d\n", input, input);
    printf("The octal equivalent of %d is %o\n", input, input);
    printf("The hexadecimal equivalent of %d is %x / %X\n", input, input, input);

    return 0;
}
#include <stdio.h>
#include <math.h>

int roundandfloor (void) {
    float input;
    int option;
    printf("1. Floor\n");
    printf("2. Round\n");
    scanf("%d", &option);
    switch (option) {
        case 1:
            printf("Enter a number to floor:\n");
            scanf("%f", &input);
            printf("%.1f floored is %.1f", input, floor(input));
            break;
        case 2:
            printf("Enter a number to round: \n");
            scanf("%f", &input);
            printf("%.1f rounded is %.1f", input, round(input));
            break;
    }

    return 0;
}
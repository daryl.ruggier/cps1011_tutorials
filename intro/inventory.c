#include <stdio.h>
#include <stdlib.h>
#include "inventory.h"



int main (void) {
    setvbuf(stdout, NULL, _IONBF, 0);
    int a3 = 1000, a4 = 1000, a5 = 1000;
    int selection, a3_order, a4_order, a5_order;
    for (int i = 0; selection = 4; i++) {
        printf("1. Order A3\n");
        printf("2. Order A4\n");
        printf("3. Order A5\n");
        printf("4. Display Order Details\n");
        printf("5. Display Updated Stock\n");
        printf("6. Exit Program\n");
        scanf("%d", &selection);
        switch (selection) {
            case 1:
                printf("Enter amount of A3 packs you would like to order: \n");
                scanf("%d", &a3_order);
                a3 -= a3_order;
                break;
            case 2:
                printf("Enter amount of A4 packs you would like to order: \n");
                scanf("%d", &a4_order);
                a4 -= a4_order;
                break;
            case 3:
                printf("Enter amount of A5 packs you would like to order: \n");
                scanf("%d", &a5_order);
                a5 -= a5_order;
                break;
            case 4:
                printf("A3 packs ordered: %d\n", 1000-a3);
                printf("A4 packs ordered: %d\n", 1000-a4);
                printf("A5 packs ordered: %d\n", 1000-a5);
                break;
            case 5:
                printf("A3 packs in stock: %d\n", a3);
                printf("A4 packs in stock: %d\n", a4);
                printf("A5 packs in stock: %d\n", a5);
                break;
            case 6:
                exit(0);
            default:
                printf("Invalid selection\n");
                main();
        }
    }


    return 0;
}

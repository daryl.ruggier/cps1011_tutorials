#include <stdio.h>

int add(int a, int b) {
    return a+b;
}

int multiply(int a, int b) {
    return a*b;
}

int subtract(int a, int b) {
    return a-b;
}

int main() {
    int a, b;
    scanf("%d %d", &a, &b);
    int addition = add(a, b);
    int subtraction = subtract(a, b);
    int multiplication = multiply(a, b);
    printf("%d + %d = %d \n", a, b, addition);
    printf("%d * %d = %d \n", a, b, multiplication);
    printf("%d - %d = %d \n", a, b, subtraction);
    return 0;
}
